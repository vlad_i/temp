﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        public static int Pow(int x, int y)
        {
            if (y > 0) return x * Pow(x, y - 1);
            if (y == 0) return 1;
            return 0;
        }

            static void Main(string[] args)
        {
            int power;
            string input = Console.ReadLine();
            int[] numbers = Array.ConvertAll(input.Split(' '), int.Parse);
            int number = Convert.ToInt32(Console.ReadLine());

            foreach (int i in numbers)
            {
                int pow = 1;
                while (true)
                {
                    if (Pow(number, pow) == i)
                    {
                        Console.WriteLine("Число " + number + " в степени " + pow + " является " + i);
                        break;
                    }
                    else if (Pow(number, pow) < i)
                    {
                        pow++;
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("Число " + i + " не является степенью числа " + number);
                        break;
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
