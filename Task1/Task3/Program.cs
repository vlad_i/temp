﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        public const int MAX_LEVEL = 10;

        class TreeFolders
        {
            public static Stack<string> folders;
            public static int level;

            public TreeFolders(string path)
            {
                folders = new Stack<string>();
                folders.Push(path);
            }

            public void CreateTreeFolders(int numberFolders)
            {
                try
                {
                    if (level > MAX_LEVEL)
                        throw new Exception("Превышен предел уровней");

                    if (numberFolders != 0)
                    {
                        string folder = folders.Pop();
                        string path = folder + "\\level_" + level;
                        string pathFile = folder + "\\file_level_" + level;

                        if (numberFolders == 1 || level == MAX_LEVEL)
                        {
                            Directory.CreateDirectory(path);
                            File.Create(pathFile).Close();
                        }
                        else
                        {
                            Directory.CreateDirectory(path);
                        }

                        folders.Push(path);
                        level++;
                        CreateTreeFolders(numberFolders - 1);
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                }
            }

            public void DeleteTreeFolders(int numberFolders)
            {
                try
                {
                    if (numberFolders > level)
                        throw new Exception("Недопустимое количество уровней для удаления. Введите число уровней меньше, чем" + level);

                    if (numberFolders != 0)
                    {
                        string folder = folders.Pop();
                        string[] files = Directory.GetFiles(folder);
                        Directory.Delete(folder, true);
                        folder = folder.Substring(0, folder.LastIndexOf("\\"));
                        folders.Push(folder);
                        level--;
                        DeleteTreeFolders(numberFolders - 1);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                }
            }
        }

        static void Main()
        {
            int foldersNumber;
            string path = @"D:\test";
            TreeFolders.level = 1;
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
                Directory.CreateDirectory(path);
            }
            else
            {
                Directory.CreateDirectory(path);
            }

            while (true)
            {
                Console.WriteLine("Выберите действие:");
                Console.WriteLine("1 - создание уровней");
                Console.WriteLine("2 - удаление уровней");
                Console.WriteLine("0 - выход");
                string option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        Console.Write("Введите количество уровней для создания: ");
                        try
                        {
                            foldersNumber = Convert.ToInt32(Console.ReadLine());
                            if (foldersNumber <= 0)
                            {
                                throw new Exception("Число уровней должно быть больше 0");
                            }

                            TreeFolders treeFolders = new TreeFolders(path);

                            if (Directory.Exists(path))
                            {
                                treeFolders.CreateTreeFolders(foldersNumber);
                                path = TreeFolders.folders.Pop();
                            }
                            else
                            {
                                throw new Exception("Такого пути не существует");
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Ошибка: " + e.Message);
                        }
                        break;
                    case "2":
                        Console.Write("Введите количество уровней для удаления: ");
                        try
                        {
                            foldersNumber = Convert.ToInt32(Console.ReadLine());
                            if (foldersNumber <= 0)
                            {
                                throw new Exception("Число уровней должно быть больше 0");
                            }

                            TreeFolders treeFoldersDel = new TreeFolders(path);

                            if (Directory.Exists(path))
                            {
                                treeFoldersDel.DeleteTreeFolders(foldersNumber);
                                path = TreeFolders.folders.Pop();
                            }
                            else
                            {
                                throw new Exception("Такого пути не существует");
                            }
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine("Ошибка: " + e.Message);
                        }
                        break;
                    case "0":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Такое действие недоступно");
                        break;
                }
            }
        }
    }
}
