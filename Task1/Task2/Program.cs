﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task2
{
    abstract class Worker
    {
        public int id;
        public string name;
        public double salary;

        public abstract void AvgSalary();
    }

    class HourWorker : Worker
    {
        public int workingHours;
        public double payPerHour;

        public override void AvgSalary()
        {
            this.salary = workingHours * payPerHour;
        }
    }

    class FixedWorker : Worker
    {
        public double payPerMonth;

        public override void AvgSalary()
        {
            this.salary = payPerMonth;
        }
    }

    class Program
    {
        static void CheckSalary(double salary)
        {

        }

        static void Main(string[] args)
        {
            var workers = new List<Worker>();
            bool flag = true;
            int id = 1;
            int N = 1;
            string pattern = @"/^[a-zA-Zа-яА-Я]+$/";
            Regex regex = new Regex(pattern);
            Match match;
            
            while (flag) { 

                Console.WriteLine("Какого работника вы хотите добавить:");
                Console.WriteLine("1 - с почасовой оплатой труда");
                Console.WriteLine("2 - с фиксированной оплатой труда");
                Console.WriteLine("0 - завершить ввод сотрудников");

                string option = Console.ReadLine();
                try
                {
                    switch (option)
                    {
                        case "1":
                            HourWorker hworker = new HourWorker();
                            hworker.id = id;
                            Console.Write("Введите имя сотрудника: ");
                            hworker.name = Console.ReadLine();
                            match = regex.Match(hworker.name);
                            if(!match.Success)
                                throw new Exception("Имя работника должно содержать только латиницу или кирилицу");
                            Console.Write("Введите количество рабочих часов: ");
                            hworker.workingHours = Convert.ToInt32(Console.ReadLine());
                            if (hworker.workingHours <= 0)
                                throw new Exception("Количество рабочих часов должно быть больше 0");
                            Console.Write("Введите оплату за час: ");
                            hworker.payPerHour = Convert.ToDouble(Console.ReadLine());
                            if (hworker.payPerHour <= 0)
                                throw new Exception("Оплата за час должна быть больше 0");
                            hworker.AvgSalary();
                            workers.Add(hworker);
                            break;
                        case "2":
                            FixedWorker fworker = new FixedWorker();
                            fworker.id = id;
                            Console.Write("Введите имя сотрудника: ");
                            fworker.name = Console.ReadLine();
                            match = regex.Match(fworker.name);
                            if (!match.Success)
                                throw new Exception("Имя работника должно содержать только латиницу или кирилицу");
                            Console.Write("Введите оплату за месяц: ");
                            fworker.payPerMonth = Convert.ToDouble(Console.ReadLine());
                            if (fworker.payPerMonth <= 0)
                                throw new Exception("Оплата за месяц должна быть больше 0");
                            fworker.AvgSalary();
                            workers.Add(fworker);
                            break;
                        case "0":
                            flag = false;
                            break;
                        default:
                            Console.WriteLine("Такой команды не существует");
                            break;
                    }
                    id++;
                }
                catch(Exception e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                }
            }

            var result1 = workers.OrderByDescending(w => w.salary).ThenBy(w => w.name);
            Console.WriteLine();
            foreach (Worker w in result1)
                Console.WriteLine("{0}. {1} - {2:2f}$", w.id, w.name, w.salary);
            Console.WriteLine();

            while (true) {
                Console.Write("Введите количество сотрудников, которое вы хотите вывести: ");
                try
                {
                    N = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                }
            }

            var result2 = result1.Take(N);
            Console.WriteLine();
            foreach (Worker w in result2)
                Console.WriteLine("{0}. {1} - {2}$", w.id, w.name, w.salary);
            Console.ReadKey();
        }
    }
}
