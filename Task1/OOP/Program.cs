﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace OOP
{
    struct Point
    {
        public int x;
        public int y;
    }

    abstract class Figure
    {
        public Point[] coord;

        public abstract void PrintFigure(Point[] coords);
        public abstract bool IsFigure(Point[] coords);
        public bool isFig(Point[] points)
        {
            if (points.Length == 4 && (!points[0].x.Equals(points[1].x) ||
                !points[0].x.Equals(points[2].x) || !points[0].x.Equals(points[3].x)))
                return true;             
            return false;
        }
    }

    class Square : Figure
    {
        public override bool IsFigure(Point[] coords)
        {
            if (coords[0].x.Equals(coords[2].x) && coords[1].x.Equals(coords[3].x) &&
               coords[0].y.Equals(coords[1].y) && coords[2].y.Equals(coords[3].y))
            {
                if (coords[0].x.Equals(coords[2].x) && coords[0].y.Equals(coords[1].y))
                {
                    Console.WriteLine("This is Square");
                    return true;
                }
            }
            return false;
        }

        public override void PrintFigure(Point[] coords)
        {
            int width = Math.Abs(coords[1].x - coords[0].x);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < width; j++)
                    Console.Write("* ");
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }

    class Rectangle : Figure
    {
        public override bool IsFigure(Point[] coords)
        {
            if (coords[0].x.Equals(coords[2].x) && coords[1].x.Equals(coords[3].x) &&
               coords[0].y.Equals(coords[1].y) && coords[2].y.Equals(coords[3].y) &&
               !coords[0].x.Equals(coords[2].x) && !coords[0].y.Equals(coords[1].y))
            {
                Console.WriteLine("This is Rectangle");
                return true;
            }
               
            return false;
        }

        public override void PrintFigure(Point[] coords)
        {
            int width = Math.Abs(coords[1].x - coords[0].x);
            int height = Math.Abs(coords[2].y - coords[0].y);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                    Console.Write("* ");
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }

    class Program
    {
        public static void WorkWithCollection(List<Figure> figures, Figure fig)
        {
            Console.WriteLine("Выберите действие:");
            Console.WriteLine("1 - добавить фигуру в коллекцию");
            Console.WriteLine("2 - распечатать фигуры из коллекции");
            Console.WriteLine("3 - выход");
            string option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    figures.Add(fig);
                    break;
                case "2":
                    Console.WriteLine();
                    foreach (var figure in figures)
                        figure.PrintFigure(figure.coord);
                    break;
                case "3":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Такой команды нет в списке");
                    break;
            }
        }

        static void Main(string[] args)
        {
            var figures = new List<Figure>();
            string input = "";
            while (true)
            {
                Point[] coord = new Point[4];
                Console.WriteLine("Введите координаты фигуры:");
                try
                {
                    for (int i = 0; i < coord.Length; i++)
                    {
                        input = Console.ReadLine();
                        coord[i].x = Array.ConvertAll(input.Split(' '), int.Parse)[0];
                        coord[i].y = Array.ConvertAll(input.Split(' '), int.Parse)[1];
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                }
                

                Console.WriteLine("Выберите фигуру:");
                Console.WriteLine("1 - прямоугольник");
                Console.WriteLine("2 - квадрат");
                string option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        Rectangle rect = new Rectangle();
                        if (rect.isFig(coord))
                        {
                            if (rect.IsFigure(coord))
                            {
                                rect.coord = coord;
                                rect.PrintFigure(coord);
                                WorkWithCollection(figures, rect);
                            }
                        }
                        break;
                    case "2":
                        Square sq = new Square();
                        if (sq.isFig(coord))
                        {
                            if (sq.IsFigure(coord))
                            {
                                sq.coord = coord;
                                sq.PrintFigure(coord);
                                WorkWithCollection(figures, sq);
                            }
                        }
                        break;
                    default:
                        Console.WriteLine("Такой фигуры нет в списке");
                        break;
                }
            } 
        }
    }
}
