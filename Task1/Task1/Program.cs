﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static string[] sot = new string[] {"сто","двести","триста","четыреста", "пят","шест", "сем","восем","девят" };
        static string[] desiat = new string[] {" двадцать"," тридцать"," сорок"," пят"," шест", " сем"," восем"," девяносто" };
        static string[] edm = new string[] {""," один"," два"," три"," четыре"," пять"," шесть", " семь"," восемь",
             " девять"," десять"," один"," две"," три",
             " четыр"," пят"," шест"," сем"," восем"," девят" };

        public class ReverseComparer : IComparer
        {
            public int Compare(Object x, Object y)
            {
                return (new CaseInsensitiveComparer()).Compare(y, x);
            }
        }

        public static void ShowMenu()
        {
            Console.WriteLine("Нажмите одну из следующих цифр для выполнения нужной операции:");
            Console.WriteLine("1 - нахождение минимального элемента массива");
            Console.WriteLine("2 - нахождение максимального элемента массива");
            Console.WriteLine("3 - нахождение суммы элементов массива");
            Console.WriteLine("4 - сортировка элементов массива по убыванию");
            Console.WriteLine("5 - сортировка элементов массива по возрастанию");
            Console.WriteLine("6 - вывод всех элементов массива");
            Console.WriteLine("7 - перевести в тип double");
            Console.WriteLine("9 - очистить консоль");
            Console.WriteLine("0 - выход");
        }

        public static double FindSum<T>(T[] array)
        {
            double sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += (dynamic)array[i];
            }

            return sum;
        }

        public static void ShowDoubleArray(double[] array)
        {
            for (int i = 0; i < array.Length; i++)
                Console.Write("{0}  ", array[i]);
            Console.WriteLine("\n");
        }

        public static void ShowIntArray(int[] array)
        {
            string[] result = new string[array.Length];
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.Length; i++)
            {
                sb.Clear();
                if (array[i] % 1000 == 0)
                    sb.Append("Тысяча");
                int ns = array[i] / 100;
                if (ns > 0)
                    sb.Append(sot[ns - 1]);
                if (ns >= 5)
                    sb.Append("ьсот");

                int R = array[i] % 100;  // < 100
                int nd = R / 10;  // Десятки
                if (nd >= 2)
                {
                    sb.Append(desiat[nd - 2]);
                    if (nd >= 5 && nd <= 8) sb.Append("ьдесят");
                    R = R % 10;
                }
                sb.Append(edm[R]);
                if (R > 10)
                    sb.Append("надцать");
                if (i != array.Length - 1)
                    Console.Write(sb.ToString() + ", ");
                else
                    Console.WriteLine(sb.ToString() + "\n");
            }
        }

        public static double[] CreateDoubleArray<T>(T[] array)
        {
            double[] newArray = new double[array.Length];
            int arrayMax = (int)(dynamic)array.Max();
            int arrayMin = (int)(dynamic)array.Min();

            try
            {
                if (arrayMin == 0)
                    throw new Exception("Деление на ноль");
                for (int i = 0; i < array.Length; i++)
                {
                    newArray[i] = (double)((dynamic)array[i] + arrayMax) / (double)arrayMin;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Ошибка: " + e.Message);
                for (int i = 0; i < array.Length; i++)
                {
                    newArray[i] = 0;
                }
            }

            return newArray;
        }

        public static double[] Start<T>(T[] array, IComparer revComparer)
        {
            bool flag = true;
            double[] arrayNew = new double[array.Length];

            while (flag)
            {
                ShowMenu();
                string option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        Console.WriteLine("{0}", array.Min());
                        break;
                    case "2":
                        Console.WriteLine("{0}", array.Max());
                        break;
                    case "3":
                        Console.WriteLine(FindSum(array));
                        break;
                    case "4":
                        Array.Sort(array, revComparer);
                        break;
                    case "5":
                        Array.Sort(array);
                        break;
                    case "6":
                        if (array is int[])
                            ShowIntArray((dynamic)array);
                        else
                            ShowDoubleArray((dynamic)array);
                        break;
                    case "7":
                        arrayNew = CreateDoubleArray(array);
                        flag = false;
                        break;
                    case "9":
                        Console.Clear();
                        break;
                    case "0":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Такой операции нет. Выберите операцию из списка ниже");
                        break;
                }
            }
            return arrayNew;
        }

        static void Main(string[] args)
        {
            int arraySize = 5;
            IComparer revComparer = new ReverseComparer();

            Console.WriteLine("Введите размер массива:");

            while (true)
            {
                try
                {
                    arraySize = Convert.ToInt32(Console.ReadLine());
                    if(arraySize <= 0)
                        throw new Exception("Размер массива должен быть положительным числом");
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Введено некорретное значение: " + e.Message);
                }
            }

            int[] array = new int[arraySize];
            double[] array2 = new double[arraySize];
            Random rand = new Random();

            for(int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(0, 1000);
            }

            array2 = Start(array, revComparer);
            while (true)
                array2 = Start(array2, revComparer);
        }
    }
}
